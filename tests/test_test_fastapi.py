from test_fastapi import __version__


def test_version():
    """Verifica la versión"""
    assert __version__ == "0.1.0"
