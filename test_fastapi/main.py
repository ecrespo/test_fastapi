# https://python.plainenglish.io/python-rest-api-tutorial-getting-started-with-fastapi-29ddb1de3f6b

from typing import Optional

from fastapi import FastAPI
from pydantic import BaseModel

app = FastAPI()


class Curso(BaseModel):
    """Estructura de datos Curso"""

    nombre: str
    descripcion: Optional[str] = None
    precio: int
    autor: Optional[str] = None


@app.get("/")
def root():
    """Retorna un mensaje de hola mundo"""
    return {"mensaje": "Hola mundo!"}


@app.get("/cursos/{nombre_curso}")
def get_curso(nombre_curso):
    """Retorna un mensaje de nombre de curso"""
    return {"nombre_curso": nombre_curso}


cursos = [
    {"nombre_curso": "Python"},
    {"nombre_curso": "SQLAlchemy"},
    {"nombre_curso": "Linux"},
]


@app.get("/cursos/")
def get_cursos(start: int, end: int):
    """retorna elementos de los cursos"""
    return cursos[start : start + end]  # noqa: E203


@app.post("/cursos/")
def post_curso(curso: Curso):
    """Crea un curso"""
    return curso
